## Simple-CDD Profiles

| Profile | Description |
| ------- | ----------- |
| `debian-i386-rt.conf` | Debian x86 Preempt-RT netinst |
| `debian-amd64-rt.conf` | Debian x64 Preempt-RT netinst |
